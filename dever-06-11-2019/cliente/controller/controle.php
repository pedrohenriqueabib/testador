<?php
require '../model/registro.php';

$post = file_get_contents('php://input');
$post = (array)json_decode($post);

class Controle{
    private $registro;

    public function __construct(){
        $this->registro = new Registro();
    }

    public function registrar($nome, $cpf){
        $this->registro->cadastrarCliente($nome, $cpf);
    }
}

$c = new Controle();
$c->registrar($post['nome'], $post['cpf']);

echo $post['nome'];

?>