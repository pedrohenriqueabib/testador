<?php
    class Registro{

        private $pdo;

        public function __construct(){
            $this->pdo = new PDO('mysql:host=localhost;dbname=registro', 'root', '');
        }

        public function listarClientes(){
            try{
                $pd = $this->pdo->prepare("SELECT * FROM cliente");
                $pd->execute();
                $p = $pd->fetchAll();
                return $p;
            }catch(PDOException $e){
                echo 'erro -> ' . $e.getMessage();
                die;
            }
            // }catch(PDOException $e){
            //     throw new ColecaoException($e->getMessage(), $e->getCode(), $e );
            // }
        }

        public function cadastrarCliente($nome, $cpf){
            try{
                $pd = $this->pdo->prepare("INSERT INTO cliente(nome, cpf) VALUES(?,?)");
                $pd->execute(array($nome, $cpf));
            }catch(PDOException $e){
                echo 'erro -> ' . $e.getMessage();
                die;
            }
            
        }

        public function alterarCliente($nome, $cpf, $limiteCredito, $id){
            try{
                $pd = $this->pdo->prepare("ALTER TABLE cliente SET nome=?, cpf=?, limiteCredito=? WHERE id=?");
                $pd->execute(array($nome, $cpf, $limiteCredito, $id));
            }catch(PDOException $e){
                echo 'erro -> ' . $e.getMessage();
                die;
            }
            
        }

        public function selecionar($id){
            try{
                $pd = $this->pdo->prepare("SELECT * FROM cliente WHERE id=?");
                $pd->execute(array($id));            
                $p = $pd->fetchAll();
                return $p;
            }catch(PDOException $e){
                echo 'erro -> ' . $e.getMessage();
                die;
            }
        }

        public function deletar($id){
            try{
                $pd = $this->pdo->prepare("DELETE FROM cliente WHERE id=?");
                $pd->execute(array($id));
            }catch(PDOException $e){
                echo 'erro -> ' . $e.getMessage();
                die; 
            }
        }
    }

    // $c = new Registro();
    // $c->cadastrarCliente('Pedro4', '12345678980');
    // $lista = $c->listarClientes();

    // foreach($lista as $l){
    //     echo $l['nome'];
    // }


?>