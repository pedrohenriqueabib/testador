import { Roteador } from "./roteador.js";
import ROTAS from "./rotas.js";

const mudarConteudo = novoConteudo => {
    let elemento = document.querySelector( '#conteudo' );
    if ( ! elemento ) { alert( 'Conteúdo não encontrado.' ); return; }
    // elemento.innerHTML = novoConteudo;
    // Remove todos os elementos atuais
    while ( elemento.firstChild ) {
        elemento.removeChild( elemento.firstChild );
    }
    // Adiciona o novo elemento
    elemento.appendChild(
        document.createRange().createContextualFragment( novoConteudo )
    );
};

const onLoad = async () => {
    try {
        const roteador = new Roteador( ROTAS, mudarConteudo, '/404' );
        await roteador.irPara( location.pathname );
    } catch ( e ) {
        mudarConteudo( e.message );
    }
};

window.addEventListener( 'load', onLoad );