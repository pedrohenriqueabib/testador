console.log( 'cliente-form.js' );
const url = location.pathname;
if ( url.match( /^\/clientes\/novo\/?$/ ) ) {
    console.log( 'novo' );

    let ok = document.getElementById('ok');
    ok.addEventListener('click', ()=>{
        let nome = document.getElementById('nome').value;
        let cpf = document.getElementById('cpf').value;

        fetch('./../../controller/controle.php', {method:'POST', body:JSON.stringify({nome, cpf})})
            .then(resposta => resposta.text())
            .then(resp => console.log(resp));
       
    });

} else if ( url.match( /^\/clientes\/[0-9]+\/alterar\/?$/ ) ) {
    const id = Number( /[0-9]+/.exec( url )[ 0 ] );
    console.log( 'alterar', id );
}