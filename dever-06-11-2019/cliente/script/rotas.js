import { Rota } from "./rota.js";

const html = 'http://localhost';
const script = `.`; // Caminho relativo ao arquivo atual

const rotas = [
    new Rota( /^\/clientes\/?$/, `${html}/clientes.html`, `${script}/cliente/clientes.js` ),
    new Rota( /^\/clientes\/novo\/?$/, `${html}/cliente-form.html`, `${script}/cliente/cliente-form.js` ),
    new Rota( /^\/clientes\/[0-9]+\/alterar\/?$/, `${html}/cliente-form.html`, `${script}/cliente/cliente-form.js` ),
    new Rota( /^\/?$/, `${html}/home.html`, `${script}/home/home.js` ),
    new Rota( /^\/404\/?$/, `${html}/404.html` )
];

export default rotas;