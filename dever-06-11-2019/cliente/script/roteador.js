export class Roteador {

    constructor( rotas, mudarConteudo, caminho404 ) {
        this.rotas = rotas;
        this.mudarConteudo = mudarConteudo;
        this.rota404 = rotas.find( r => r.er.test( caminho404 ) );
        if ( ! this.rota404 ) {
            throw new Error( 'Rota 404 não encontrada.' );
        }
    }

    async irPara( url ) {
        let rota = this.rotas.find( r => r.er.test( url ) );
        if ( ! rota ) {
            rota = this.rota404;
        }
        if ( rota.html ) {
            try {
                const resultado = await fetch( rota.html );
                const conteudo = await resultado.text();
                if ( resultado.ok ) {
                    this.mudarConteudo.call( this, conteudo );
                } else {
                    throw new Error( conteudo );
                }
            } catch ( e ) {
                this.mudarConteudo.call( this, e.message );
            }
        }

        if ( rota.js ) {
            try {
                await import( rota.js );
            } catch ( e ) {
                this.mudarConteudo.call( this, e.message );
            }
        }
    }

}